/*************************************************
  SUPPRESSION DES TABLES
**************************************************/
-- Si désiré

/*************************************************
  CRÉATION DES TABLES
**************************************************/
CREATE TABLE RM_Pays
(
  id	number(7) NOT NULL,
  nom   varchar2(50) NOT NULL
);

CREATE TABLE RM_Votes_Films_EnAttente
(
  idMembre 		number(7) NOT NULL,
  idAttente 	number(7) NOT NULL,
  pourContre	char(1) NOT NULL
);

CREATE TABLE RM_Votes_MurFilms
(
  idMembre 		number(7) NOT NULL,
  idMessage 	number(7) NOT NULL,
  pourContre	char(1) NOT NULL
);

CREATE TABLE RM_FilmsFavoris_Membres
(
  idMembre 		number(7) NOT NULL,
  idFilm	 	number(7) NOT NULL
);

CREATE TABLE RM_Films
(
  id        		number(7) NOT NULL,
  titre             varchar2(100) NOT NULL,
  dateParution      date NOT NULL,
  description       varchar2(500),
  synopsis			varchar2(2000) NOT NULL,
  budget			number(10,2),
  profits			number(11,2),
  pointage			number(2),
  dateModification	date,
  splashImage		varchar2(50) NOT NULL,
  nbVisitesPage		number(6)
);


CREATE TABLE RM_GenresFilms
(
  idFilm 		number(7) NOT NULL,
  idGenre	 	number(7) NOT NULL
);

CREATE TABLE RM_Genres
(
  id 		number(7) NOT NULL,
  nom	 	varchar2(40) NOT NULL
);

CREATE TABLE RM_Roles
(
  id 		number(7) NOT NULL,
  nom	 	varchar2(40) NOT NULL
);

CREATE TABLE RM_ChangementsEnAttente_Films
(
  id        			number(7) NOT NULL,
  idFilm       			number(7) NOT NULL,
  titre             	varchar2(100) NOT NULL,
  dateParution      	date NOT NULL,
  description       	varchar2(500),
  synopsis				varchar2(2000) NOT NULL,
  budget				number(10,2),
  profits				number(11,2),
  pointage				number(2),
  splashImage			varchar2(50) NOT NULL,
  dateAjout				date NOT NULL,
  idMembreModification	number(7) NOT NULL,
  actif					char(1) NOT NULL
);


CREATE TABLE RM_Membres
(
  id      		number(7) NOT NULL,
  login    		varchar(50) NOT NULL,
  motPasse    	varchar2(100) NOT NULL,
  email         varchar2(100),
  prenom        varchar2(50) NOT NULL,
  nom           varchar2(50) NOT NULL,
  idPays      	number(7) NOT NULL,
  dateNaissance	date NOT NULL,
  sexe			char(1) NOT NULL,
  siteWeb		varchar2(200) 
);



CREATE TABLE RM_Messages_MurFilms
(
-- À COMPLÉTER
);


CREATE TABLE RM_Personnes
(
  id 			number(7) NOT NULL,
  prenom        varchar2(50) NOT NULL,
  nom           varchar2(50) NOT NULL,
  dateNaissance	date NOT NULL,
  sexe			char(1) NOT NULL
);

CREATE TABLE RM_Employes
(
  idRole 		number(7) NOT NULL,
  idPersonne 	number(7) NOT NULL,
  idFilm	 	number(7) NOT NULL
);

CREATE TABLE RM_Critiques
(
-- À COMPLÉTER
);



/*************************************************
  CRéATION DES INDEX
**************************************************/
-- À COMPLÉTER


/*************************************************
  CRéATION DES SéQUENCES
**************************************************/
-- À COMPLÉTER


/*************************************************
  INSERTION DES DONNéES
**************************************************/
-- À COMPLÉTER

INSERT INTO RM_Genres VALUES (1, 'Comédie');
INSERT INTO RM_Genres VALUES (2, 'Drame');
INSERT INTO RM_Genres VALUES (3, 'Horreur');
INSERT INTO RM_Genres VALUES (4, 'Musical');

INSERT INTO RM_Pays VALUES (1, 'Canada');
INSERT INTO RM_Pays VALUES (2, 'Italie');
INSERT INTO RM_Pays VALUES (3, 'France');
INSERT INTO RM_Pays VALUES (4, 'Espagne');
INSERT INTO RM_Pays VALUES (5, 'Mexique');
INSERT INTO RM_Pays VALUES (6, 'États-Unis');

INSERT INTO RM_Roles VALUES (1, 'Acteur');
INSERT INTO RM_Roles VALUES (2, 'Directeur');
INSERT INTO RM_Roles VALUES (3, 'Musicien');
INSERT INTO RM_Roles VALUES (4, 'Scénariste');
INSERT INTO RM_Roles VALUES (5, 'Figurant');
INSERT INTO RM_Roles VALUES (6, 'Maquilleur');
INSERT INTO RM_Roles VALUES (7, 'Caméraman');

INSERT INTO RM_Personnes VALUES (1, 'Olivia', 'Colman', '1974-01-30', 'F');
INSERT INTO RM_Personnes VALUES (2, 'Anthony', 'Hopkins', '1937-12-31', 'H');
INSERT INTO RM_Personnes VALUES (3, 'Ryan', 'Gosling', '1980-11-12', 'H');
INSERT INTO RM_Personnes VALUES (4, 'Damien', 'Chazelle', '1985-01-19', 'H');

INSERT INTO RM_Films (id, titre, dateParution, synopsis, splashImage) VALUES (1, 'The Father', '2021-03-19', 'A man refuses all assistance from his daughter as he ages. As he tries to make sense of his changing circumstances, he begins to doubt his loved ones, his own mind and even the fabric of his reality.', 'the-father.jpg');
INSERT INTO RM_Films (id, titre, dateParution, synopsis, splashImage) VALUES (2, 'La La Land', '2016-12-25', 'While navigating their careers in Los Angeles, a pianist and an actress fall in love while attempting to reconcile their aspirations for the future. ', 'la-la-land.jpg');

INSERT INTO RM_Employes VALUES (1,1,1);
INSERT INTO RM_Employes VALUES (1,2,1);
INSERT INTO RM_Employes VALUES (1,3,2);
INSERT INTO RM_Employes VALUES (2,4,2);

INSERT INTO RM_GenresFilms VALUES (1,2);
INSERT INTO RM_GenresFilms VALUES (2,1);
INSERT INTO RM_GenresFilms VALUES (2,4);


/*************************************************
  REQUÊTES
**************************************************/
-- À COMPLÉTER


