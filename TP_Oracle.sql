/*

Noms et matricules:
-------------------

Nicolas Lavigne         2191913
Tristan Hamel           2192068

**************************************************
  SUPPRESSION DES TABLES
**************************************************/

DROP TABLE RM_Critiques;
DROP TABLE RM_Employes;
DROP TABLE RM_Personnes;
DROP TABLE RM_Roles;
DROP TABLE RM_GenresFilms;
DROP TABLE RM_Genres;
DROP TABLE RM_FilmsFavoris_Membres;
DROP TABLE RM_Votes_MurFilms;
DROP TABLE RM_Messages_MurFilms;
DROP TABLE RM_Votes_Films_EnAttente;
DROP TABLE RM_ChangementsEnAttente_Films;
DROP TABLE RM_Films;
DROP TABLE RM_Membres;
DROP TABLE RM_Pays;
DROP SEQUENCE incrementIdMembres;
DROP SEQUENCE incrementIdCritique;
DROP VIEW vue_FilmsModifiesDeuxFois;
DROP INDEX idx_RM_Critiques;
DROP INDEX idx_RM_Messages_MurFilms;
DROP INDEX idx_RM_Membres;
DROP INDEX idx_RM_ChangementsEnAttente_Films;

/*************************************************
  CREATION DES SEQUENCES
**************************************************/

CREATE SEQUENCE incrementIdMembres
    INCREMENT BY 1
    START WITH 0
    MINVALUE 0
    MAXVALUE 999999
    NOCYCLE;
    
CREATE SEQUENCE incrementIdCritique
    INCREMENT BY 1
    START WITH 0
    MINVALUE 0
    MAXVALUE 99999
    NOCYCLE;

/*************************************************
  CREATION DES TABLES
**************************************************/

SET LANGUAGE = FR;

CREATE TABLE RM_Pays (
    id   VARCHAR2(2) NOT NULL,
    nom  VARCHAR2(50) NOT NULL,
    CONSTRAINT CK_RM_Pays_LongueurId_pays CHECK(LENGTH(id) = 2),
    CONSTRAINT PK_RM_Pays_paysId PRIMARY KEY (id)
);
COMMENT ON TABLE RM_Pays IS 'Liste des pays pour les membres ou les films';


CREATE TABLE RM_Membres (
    id             NUMBER(7) DEFAULT incrementIdMembres.NEXTVAL NOT NULL,
    login          VARCHAR(50) NOT NULL,
    motPasse       VARCHAR2(100) NOT NULL,
    email          VARCHAR2(100),
    prenom         VARCHAR2(50) NOT NULL,
    nom            VARCHAR2(50) NOT NULL,
    idPays         VARCHAR2(2) DEFAULT 'CA' NOT NULL,
    dateNaissance  DATE NOT NULL,
    sexe           CHAR(1) NOT NULL,
    siteWeb        VARCHAR2(200)
);
COMMENT ON TABLE RM_Membres IS 'Liste de tous les membres';

ALTER TABLE RM_Membres ADD CONSTRAINT PK_idMembre PRIMARY KEY (id);
ALTER TABLE RM_Membres ADD CONSTRAINT CK_RM_Membres_FormatSiteWeb CHECK(REGEXP_LIKE(siteWeb, '^https://[a-zA-Z0-9-_.]{5}') OR REGEXP_LIKE(siteweb, '^http://[a-zA-Z0-9-_.]{5}') );
ALTER TABLE RM_Membres ADD CONSTRAINT CK_RM_Membres_FormatEmail CHECK(REGEXP_LIKE(email,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}'));
ALTER TABLE RM_Membres ADD CONSTRAINT CK_RM_Membres_LongueurIdpays CHECK(LENGTH(idPays) = 2);
ALTER TABLE RM_Membres ADD CONSTRAINT FK_RM_Membres_idPays FOREIGN KEY (idPays) REFERENCES RM_Pays(id);


CREATE TABLE RM_Films (
    id                NUMBER(7) NOT NULL,
    titre             VARCHAR2(100) NOT NULL,
    dateParution      DATE NOT NULL,
    description       VARCHAR2(500),
    synopsis          VARCHAR2(2000) NOT NULL,
    budget            NUMBER(10, 2),
    profits           NUMBER(11, 2),
    pointage          NUMBER(2),
    dateModification  DATE,
    splashImage       VARCHAR2(50) NOT NULL,
    nbVisitesPage     NUMBER(6),
    CONSTRAINT PK_RM_Films_films_ID PRIMARY KEY (id),
    CONSTRAINT CK_RM_Films_dateModification CHECK(datemodification >= dateparution),
    CONSTRAINT CK_RM_Films_Pointage CHECK (pointage BETWEEN 0 AND 10),
    CONSTRAINT CK_RM_Films_BudgetEstPositif CHECK(budget > 0),
    CONSTRAINT CK_RM_Films_ProfitEstPositif CHECK(profits > 0)
);

COMMENT ON TABLE RM_Films IS 'Liste de tous les films';
COMMENT ON COLUMN RM_Films.id IS 'Identifiant pour chaque film';
COMMENT ON COLUMN RM_Films.titre IS 'Le titre du film';
COMMENT ON COLUMN RM_Films.dateParution IS 'Date � laquelle le film est sortie';
COMMENT ON COLUMN RM_Films.description IS 'Une description du film de maximum 500 caract�re';
COMMENT ON COLUMN RM_Films.synopsis IS 'Description de la tramme narrative du film en maximum 2000 caract�re';
COMMENT ON COLUMN RM_Films.budget IS 'Le budget officiel pour le film';
COMMENT ON COLUMN RM_Films.profits IS 'Les profits g�n�r� par le film';
COMMENT ON COLUMN RM_Films.pointage IS 'Le pointage du film';
COMMENT ON COLUMN RM_Films.dateModification IS 'La date � laquelle un modification � �t� effectu�';
COMMENT ON COLUMN RM_Films.splashImage IS 'Image promotionnel du film';
COMMENT ON COLUMN RM_Films.nbVisitesPage IS 'Nombre de visite sur la page du film';

ALTER TABLE RM_Films
ADD (langueOriginale VARCHAR(2) DEFAULT 'FR' NOT NULL ,
     CONSTRAINT CK_langueLongueur CHECK(LENGTH(langueOriginale)= 2 ));


CREATE TABLE RM_ChangementsEnAttente_Films (
    id                    NUMBER(7) NOT NULL,
    idFilm                NUMBER(7) NOT NULL,
    titre                 VARCHAR2(100) NOT NULL,
    dateParution          DATE NOT NULL,
    description           VARCHAR2(500),
    synopsis              VARCHAR2(2000) NOT NULL,
    budget                NUMBER(10, 2),
    profits               NUMBER(11, 2),
    pointage              NUMBER(2),
    splashImage           VARCHAR2(50) NOT NULL,
    dateAjout             DATE DEFAULT SYSDATE NOT NULL,
    idMembreModification  NUMBER(7) NOT NULL,
    actif                 CHAR(1) NOT NULL,
    CONSTRAINT PK_ChangementAttente_Films_id PRIMARY KEY (id),
    CONSTRAINT FK_ChangementAttente_Films_idFilm FOREIGN KEY (idFilm) REFERENCES RM_Films (id),
    CONSTRAINT FK_ChangementAttente_Films_idMembreModification FOREIGN KEY (idMembreModification) REFERENCES RM_Membres(id),
    CONSTRAINT CK_ChangementAttente_Films_BudgetEstPositif CHECK(budget > 0),
    CONSTRAINT CK_ChangementAttente_Films_ProfitEstPositif CHECK(profits > 0),
    CONSTRAINT CK_ChangementAttente_Films_Pointage CHECK (pointage BETWEEN 0 AND 10),
    CONSTRAINT CK_ChangementAttente_Films_Actif CHECK(actif = '0' OR  actif = '1' )
);
COMMENT ON TABLE RM_ChangementsEnAttente_Films IS 'Liste de tous les changements en attente par film';


CREATE TABLE RM_Votes_Films_EnAttente (
    idMembre    NUMBER(7) NOT NULL,
    idAttente   NUMBER(7) NOT NULL,
    pourContre  CHAR(1) NOT NULL,
    CONSTRAINT PK_VoteFilmsEnAttente PRIMARY KEY(idMembre,idAttente),
    CONSTRAINT FK_Votes_EnAttente_idMembre FOREIGN KEY (idMembre) REFERENCES RM_Membres(id),
    CONSTRAINT FK_Votes_EnAttente_idAttente FOREIGN KEY (idAttente) REFERENCES RM_ChangementsEnAttente_Films(id),
    CONSTRAINT CK_Votes_EnAttente_pourContre CHECK(pourContre = '0' OR  pourContre = '1' )
);
COMMENT ON TABLE RM_Votes_Films_EnAttente IS 'Liste des films dont le vote est en attente';


CREATE TABLE RM_Messages_MurFilms
(   
    id NUMBER(7)    NOT NULL, 
    idMembre        NUMBER(7) NOT NULL,
    idFilm          NUMBER(7) NOT NULL,
    message         VARCHAR2(50) NOT NULL,
    dateAjout       DATE DEFAULT SYSDATE NOT NULL,
    CONSTRAINT PK_idMessageMurFilms PRIMARY KEY (id),
    CONSTRAINT FK_MessageMurFilms_idMembre FOREIGN KEY (idMembre) REFERENCES RM_Membres(id),
    CONSTRAINT FK_MessageMurFilms_idFilm FOREIGN KEY (idFilm) REFERENCES RM_Films(id)
);
COMMENT ON TABLE RM_Messages_MurFilms IS 'Liste des messages de MurFilms';


CREATE TABLE RM_Votes_MurFilms (
    idMembre    NUMBER(7) NOT NULL,
    idMessage   NUMBER(7) NOT NULL,
    pourContre  CHAR(1) NOT NULL,
    CONSTRAINT PK_VotesMurfilms PRIMARY KEY(idMessage,idMembre),
    CONSTRAINT FK_Votes_MurFilms_idMembre FOREIGN KEY (idMembre) REFERENCES RM_Membres(id),
    CONSTRAINT FK_Votes_MurFilms_idMessage FOREIGN KEY (idMessage) REFERENCES RM_Messages_MurFilms (id),
    CONSTRAINT CK_VotesMurfilms_PourContre CHECK(pourContre = '0' OR  pourContre = '1' )
);
COMMENT ON TABLE RM_Votes_MurFilms IS 'Liste des votes par memebre pour MurFilm';


CREATE TABLE RM_FilmsFavoris_Membres (
    idMembre  NUMBER(7) NOT NULL,
    idFilm    NUMBER(7) NOT NULL,
    CONSTRAINT PK_Filmsfavoris PRIMARY KEY(idMembre,idFilm),
    CONSTRAINT PK_Filmsfavoris_Membres_idMembre FOREIGN KEY (idMembre) REFERENCES RM_Membres(id),
    CONSTRAINT PK_Filmsfavoris_Membres_idFilm FOREIGN KEY (idFilm) REFERENCES RM_Films(id)
);
COMMENT ON TABLE RM_FilmsFavoris_Membres IS 'Liste des films favoris par membres';


CREATE TABLE RM_Genres (
    id   NUMBER(7) NOT NULL,
    nom  VARCHAR2(40) NOT NULL,
    CONSTRAINT PK_idGenre PRIMARY KEY (id)
);
COMMENT ON TABLE RM_Genres IS 'Liste de tous les genres de films';


CREATE TABLE RM_GenresFilms (
    idFilm   NUMBER(7) NOT NULL,
    idGenre  NUMBER(7) NOT NULL,
    CONSTRAINT PK_GenresFilms PRIMARY KEY(idFilm, idGenre),
    CONSTRAINT FK_GenresFilms_idFilm FOREIGN KEY (idFilm) REFERENCES RM_Films(id),
    CONSTRAINT FK_GenresFilms_idGenre FOREIGN KEY (idGenre) REFERENCES RM_Genres(id)
);
COMMENT ON TABLE RM_GenresFilms IS 'Liste des films par leurs genre';


CREATE TABLE RM_Roles (
    id   NUMBER(7) NOT NULL,
    nom  VARCHAR2(40) NOT NULL,
    CONSTRAINT PK_Roles_idRole PRIMARY KEY (id)
);
COMMENT ON TABLE RM_Roles IS 'Liste de tous les roles';


CREATE TABLE RM_Personnes
(
  id 			number(7) NOT NULL,
  prenom        varchar2(50) NOT NULL,
  nom           varchar2(50) NOT NULL,
  dateNaissance	date NOT NULL,
  sexe			char(1) NOT NULL,
  CONSTRAINT PK_idPersonne PRIMARY KEY (id),
  CONSTRAINT CK_LongueurSexe CHECK(LENGTH(sexe)= 1)
);
COMMENT ON TABLE RM_Personnes IS 'Liste de personnes avec leurs nom complet, leurs date de naissance et leurs sexe';


CREATE TABLE RM_Employes
(
  idRole 		number(7) NOT NULL,
  idPersonne 	number(7) NOT NULL,
  idFilm	 	number(7) NOT NULL,
  CONSTRAINT PK_Employes PRIMARY KEY(idRole, idPersonne, idFilm),
  CONSTRAINT FK_Employes_idRole FOREIGN KEY (idRole) REFERENCES RM_Roles(id),
  CONSTRAINT FK_Employes_idPersonne FOREIGN KEY (idPersonne) REFERENCES RM_Personnes(id),
  CONSTRAINT FK_Employes_idFilm FOREIGN KEY (idFilm) REFERENCES RM_Films(id)
);
COMMENT ON TABLE rm_Employes IS 'Liste des employes';


CREATE TABLE RM_Critiques
(
    id NUMBER(7) DEFAULT incrementIdCritique.NEXTVAL NOT NULL,
    pointageScenario NUMBER(2) NOT NULL,
    pointageDistribution NUMBER(2) NOT NULL,
    pointageSFX NUMBER(2) NOT NULL,
    pointageGlobal NUMBER(2) GENERATED ALWAYS AS ((pointageScenario + pointageDistribution + pointageSFX ) / 3) NOT NULL,
    commentaire VARCHAR2(50) NOT NULL,
    dateAjout DATE DEFAULT SYSDATE NOT NULL,
    idMembre NUMBER(2) NOT NULL,
    idFilm NUMBER(2) NOT NULL,
    CONSTRAINT CK_Critiques_PointageGobal CHECK(pointageGlobal BETWEEN 0 AND 10),
    CONSTRAINT CK_Critiques_PointageScenario CHECK(pointageScenario BETWEEN 0 AND 10),
    CONSTRAINT CK_Critiques_PointageDistribution CHECK(pointageDistribution BETWEEN 0 AND 10),
    CONSTRAINT CK_Critiques_PointageSFX CHECK(pointageSFX BETWEEN 0 AND 10),
    CONSTRAINT PK_idCritique PRIMARY KEY (id),
    CONSTRAINT FK_Critique_idMembre FOREIGN KEY (idMembre) REFERENCES RM_Membres(id),
    CONSTRAINT FK_Critique_idFilm FOREIGN KEY (idFilm) REFERENCES RM_Films(id)
);
COMMENT ON TABLE RM_Critiques IS 'Liste des critiques de film avec le pointage et les commentaires';


/*************************************************
  CREATION DES INDEX
**************************************************/
CREATE INDEX idx_RM_Critiques ON RM_Critiques(idFilm, idMembre);
CREATE INDEX idx_RM_Messages_MurFilms ON RM_Messages_MurFilms(idMembre, idFilm);
CREATE INDEX idx_RM_Membres ON RM_Membres(idPays);
CREATE INDEX idx_RM_ChangementsEnAttente_Films ON RM_ChangementsEnAttente_Films(idFilm);

/*************************************************
  INSERTION DES DONN�ES
**************************************************/

INSERT INTO RM_Genres VALUES (1, 'Com�die');
INSERT INTO RM_Genres VALUES (2, 'Drame');
INSERT INTO RM_Genres VALUES (3, 'Horreur');
INSERT INTO RM_Genres VALUES (4, 'Musical');
-- Ajout d'enregistrements
INSERT INTO RM_Genres VALUES (5, 'Documentaire');
INSERT INTO RM_Genres VALUES (6, 'Thriller');
INSERT INTO RM_Genres VALUES (7, 'Action');

INSERT INTO RM_Pays VALUES ('CA', 'Canada');
INSERT INTO RM_Pays VALUES ('IT', 'Italie');
INSERT INTO RM_Pays VALUES ('FR', 'France');
INSERT INTO RM_Pays VALUES ('ES', 'Espagne');
INSERT INTO RM_Pays VALUES ('MX', 'Mexique');
INSERT INTO RM_Pays VALUES ('US', '�tats-Unis');
-- Ajout d'enregistrements
INSERT INTO RM_Pays VALUES ('KR', 'Cor�e');
INSERT INTO RM_Pays VALUES ('UK', 'Royaume-Uni');
INSERT INTO RM_Pays VALUES ('JA', 'Jama�que');
INSERT INTO RM_Pays VALUES ('RU', 'Russie');
INSERT INTO RM_Pays VALUES ('JN', 'Japon');
INSERT INTO RM_Pays VALUES ('BE', 'Belgique');

INSERT INTO RM_Roles VALUES (1, 'Acteur');
INSERT INTO RM_Roles VALUES (2, 'Directeur');
INSERT INTO RM_Roles VALUES (3, 'Musicien');
INSERT INTO RM_Roles VALUES (4, 'Sc�nariste');
INSERT INTO RM_Roles VALUES (5, 'Figurant');
INSERT INTO RM_Roles VALUES (6, 'Maquilleur');
INSERT INTO RM_Roles VALUES (7, 'Cam�raman');
-- Ajout d'enregistrements
INSERT INTO RM_Roles VALUES (8, 'Cascadeur');
INSERT INTO RM_Roles VALUES (9, 'R�alisateur');

INSERT INTO RM_Personnes VALUES (1, 'Olivia', 'Colman', '1974-01-30', 'F');
INSERT INTO RM_Personnes VALUES (2, 'Anthony', 'Hopkins', '1937-12-31', 'H');
INSERT INTO RM_Personnes VALUES (3, 'Ryan', 'Gosling', '1980-11-12', 'H');
INSERT INTO RM_Personnes VALUES (4, 'Damien', 'Chazelle', '1985-01-19', 'H');
-- Ajout d'enregistrements
INSERT INTO RM_Personnes VALUES (5, 'Mathieu', 'Kassovitz', '1967-08-03', 'H');
INSERT INTO RM_Personnes VALUES (6, 'Vincent', 'Cassel', '1967-11-23', 'H');
INSERT INTO RM_Personnes VALUES (7, 'Marc-Andre', 'Leclerc', '1967-11-23', 'H');
INSERT INTO RM_Personnes VALUES (8, 'Frances', 'McDormand', '1957-06-23', 'F');
INSERT INTO RM_Personnes VALUES (9, 'Rob', 'Schneider', '1963-10-31', 'H');
INSERT INTO RM_Personnes VALUES (10, 'Tom', 'Cruise', '1962-07-3', 'H');
INSERT INTO RM_Personnes VALUES (11, 'Woody', 'Harrelson', '1961-06-23', 'H');
INSERT INTO RM_Personnes VALUES (12, 'Tristan', 'Hamel', '1962-07-12', 'H');

INSERT INTO RM_Films (id, titre, dateParution, synopsis, budget, profits, splashImage) VALUES (1, 'The Father', '2021-03-19', 'A man refuses all assistance from his daughter as he ages. As he tries to make sense of his changing circumstances, he begins to doubt his loved ones, his own mind and even the fabric of his reality.',5000000, 5340000, 'the-father.jpg');
INSERT INTO RM_Films (id, titre, dateParution, synopsis, budget, profits, splashImage) VALUES (2, 'La La Land', '2016-12-25', 'While navigating their careers in Los Angeles, a pianist and an actress fall in love while attempting to reconcile their aspirations for the future. ',9000000, 15500200, 'la-la-land.jpg');
-- Ajout d'enregistrements
INSERT INTO RM_Films (id, titre, dateParution, synopsis, budget, profits, splashImage) VALUES (3, 'Les Rivi�res Pourpres', '2000-09-27', 'Deux policiers pourchassent un tueur d�ment qui pars�me une petite ville universitaire de corps mutil�s.',14000000, 46103680, 'les-rivi�res-pourpres.jpg');
INSERT INTO RM_Films (id, titre, dateParution, synopsis, budget, profits, splashImage) VALUES (4, 'La Haine', '1995-09-01', 'Une �meute �clate dans la cit� des Muguets, � la suite d''une bavure polici�re. Au petit matin, trois copains se retrouvent.',2590000, 416188, 'la-haine.jpg');
INSERT INTO RM_Films (id, titre, dateParution, synopsis, budget, profits, splashImage) VALUES (5, 'Resident Evil: Bienvenue � Raccon City', '2021-11-24', 'Autrefois le si�ge en plein essor du g�ant pharmaceutique Umbrella Corporation, Raccoon City est aujourd''hui une ville � l''agonie. L''exode de la soci�t� a laiss� la ville en friche... et un grand mal se pr�pare sous la surface. Lorsque celui-ci se d�cha�ne, les habitants de la ville sont � jamais... chang�s... et un petit groupe de survivants doit travailler ensemble pour d�couvrir la v�rit� sur Umbrella et survivre � la nuit.',10000000, 13900000,'resident-evil.jpg');
INSERT INTO RM_Films (id, titre, dateParution, synopsis, budget, profits, splashImage) VALUES (6,'The Alpinist','2021-11-15','Marc-Andr� Leclerc grimpe seul. L''esprit libre de 23 ans r�alise certaines des ascensions en solo les plus audacieuses de l''histoire. Sans marge d''erreur, l''approche de Leclerc est lessence m�me de l''aventure en solo.',500000 ,1264597, 'Alpinist.jpg');
INSERT INTO RM_Films (id, titre, dateParution, synopsis, budget, profits, splashImage) VALUES (7,'3 Billboards: Les panneaux de la vengeance','2018-1-17','Une m�re prend les choses en main lorsque les autorit�s locales ne parviennent pas � r�soudre le meurtre de sa fille et attraper le coupable.', 10000000, 14000000, '3Billboard.jpg');
INSERT INTO RM_Films (id, titre, dateParution, synopsis, budget, profits, splashImage) VALUES (8,'Deuce Bigalow: Gigolo a tout prix','1999-12-10','Un nettoyeur d''aquarium ordinaire garde la maison d''un gigolo, seulement pour �tre contraint d''en devenir un lui-m�me.',5000000, 23000000, 'DeuceBigalow.jpg');
INSERT INTO RM_Films (id, titre, dateParution, synopsis, budget, profits, splashImage) VALUES (9,'Mission Impossibe: Protocole fantome','2011-12-14','The IMF is shut down when it''s implicated in the bombing of kremlin, causing Ethan Hunt and his new team to go rogue to clear their organisations''s name', 20000000, 34000000, 'MIIghost.jpg');
INSERT INTO RM_Films (id, titre, dateParution, synopsis, budget, profits, splashImage) VALUES (10,'Mission Impossibe 3','2006-05-03','Ethan Hunt, agent de Mission Impossible, entre en conflit avec un trafiquant d''armes dangereux et sadique qui menace sa vie et celle de sa fiancee', 12345678,45000000, 'MI3.jpg');
INSERT INTO RM_Films (id, titre, dateParution, synopsis, budget, profits, splashImage) VALUES (11,'Deuce Bigalow: Gigolo malgr� lui','2005-08-14','Un nettoyeur d''aquarium ordinaire garde la maison d''un gigolo, seulement pour �tre contraint d''en devenir un lui-m�me.',5000000, 23000000, 'DeuceBigalow2.jpg');

INSERT INTO RM_Employes VALUES (1,1,1);
INSERT INTO RM_Employes VALUES (1,2,1);
INSERT INTO RM_Employes VALUES (1,3,2);
INSERT INTO RM_Employes VALUES (2,4,2);
-- Ajout d'enregistrements
INSERT INTO RM_Employes (idRole, idPersonne, idFilm) VALUES (1,5,3);   -- Mathieu Kassovitz in Les rivieres pourpres
INSERT INTO RM_Employes (idRole, idPersonne, idFilm) VALUES (1,6,4);   -- Vincent Casset in La Haine
INSERT INTO RM_Employes (idRole, idPersonne, idFilm) VALUES (1,7,6);   -- Marc-Andre Leclerc in The Apliniste
INSERT INTO RM_Employes (idRole, idPersonne, idFilm) VALUES (1,8,7);   -- Frances McDormand in 3 BillBoards
INSERT INTO RM_Employes (idRole, idPersonne, idFilm) VALUES (1,11,7);  -- Woody Harelsson in 3 Billboards
INSERT INTO RM_Employes (idRole, idPersonne, idFilm) VALUES (1,9,8);   -- Rob Shenider in Deuce Bigolo
INSERT INTO RM_Employes (idRole, idPersonne, idFilm) VALUES (1,10,9);  -- Tom Cruise in MI Ghost Protocole
INSERT INTO RM_Employes (idRole, idPersonne, idFilm) VALUES (1,10,10); -- Tom Cruise in MI 3
INSERT INTO RM_Employes (idRole, idPersonne, idFilm) VALUES (1,9,11);   -- Rob Shenider in Deuce Bigolo

INSERT INTO RM_GenresFilms VALUES (1,2);
INSERT INTO RM_GenresFilms VALUES (2,1);
-- Ajout d'enregistrements
INSERT INTO RM_GenresFilms VALUES (3,6);  -- Les rivieres pourpre, Thriller
INSERT INTO RM_GenresFilms VALUES (4,2);  -- La Haine, Drame
INSERT INTO RM_GenresFilms VALUES (5,3);  -- Resident Evil, Horreur
INSERT INTO RM_GenresFilms VALUES (6,5);  -- The alpinist, Documentaire
INSERT INTO RM_GenresFilms VALUES (7,2);  -- 3 billboard, Drame
INSERT INTO RM_GenresFilms VALUES (8,1);  -- Deuce bigolo, Comedie
INSERT INTO RM_GenresFilms VALUES (9,7);  -- Mission Impossible Ghost, Action
INSERT INTO RM_GenresFilms VALUES (10,7); -- Mission Impossible 3, Action
INSERT INTO RM_GenresFilms VALUES (11,1);  -- Deuce bigolo, Comedie

-- Autres Ajout d'enregistrements
INSERT INTO RM_Membres (id, login, motPasse, email, prenom, nom, idPays, dateNaissance, sexe) VALUES (1, 'admin', 'admin', 'admin@gmail.com', 'Boby', 'Marley', 'JA', '1945-02-06', 'H');
INSERT INTO RM_Membres (id, login, motPasse, email, prenom, nom, idPays, dateNaissance, sexe, siteWeb) VALUES (2, 'wako', 'yuku', 'wako@gmail.com', 'Zak', 'Mock', 'CA', '1984-07-06', 'H', 'https://www.waku.com');
INSERT INTO RM_Membres (id, login, motPasse, email, prenom, nom, idPays, dateNaissance, sexe) VALUES (3, 'XxBoaxX', 'tondeuse', 'BoaConstructor@hotmail.com', 'Boa', 'Conway', 'FR', '1973-03-08', 'H');
INSERT INTO RM_Membres (id, login, motPasse, email, prenom, nom, idPays, dateNaissance, sexe) VALUES (4, 'yann', 'soleil', 'MiaWallace@hotmail.com', 'Yann', 'Martel', 'CA', '1991-04-12', 'H');
INSERT INTO RM_Membres (id, login, motPasse, email, prenom, nom, idPays, dateNaissance, sexe) VALUES (5, 'PopCorn', 'piscine', 'crackAddict@gmail.com', 'John', 'Marston', 'US', '1989-06-12', 'H');
INSERT INTO RM_Membres (id, login, motPasse, email, prenom, nom, idPays, dateNaissance, sexe) VALUES (6, 'bob', 'sopalin', 'jabsorbe@gmail.com', 'Bob', 'Leponge', 'CA', '1982-03-15', 'H');
INSERT INTO RM_Membres (id, login, motPasse, email, prenom, nom, idPays, dateNaissance, sexe) VALUES (7, 'zoe', 'jesus', 'sainte@gmail.com', 'Zoe', 'Marie', 'KR', '1990-09-11', 'H');
INSERT INTO RM_Membres (id, login, motPasse, email, prenom, nom, idPays, dateNaissance, sexe) VALUES (8, 'marco', 'lessive', 'omo@gmail.com', 'Marco', 'Bianco', 'ES', '1987-06-30', 'H');
INSERT INTO RM_Membres (id, login, motPasse, email, prenom, nom, idPays, dateNaissance, sexe) VALUES (9, 'john', 'lipton', 'teatime@gmail.com', 'John', 'Smith', 'FR', '1981-12-12', 'H');
INSERT INTO RM_Membres (id, login, motPasse, email, prenom, nom, idPays, dateNaissance, sexe) VALUES (10, 'ali', 'tobogan', 'walibi@gmail.com', 'Ali', 'Walli', 'UK', '2001-01-01', 'H');

INSERT INTO RM_ChangementsEnAttente_Films (id, idFilm, titre, dateParution, synopsis, splashImage, dateAjout, idMembreModification, actif) VALUES (1, 1, 'Alien', '1979-05-25', 'synopsis', 'alien.jpg', '2021-11-25', 1, '1');
INSERT INTO RM_ChangementsEnAttente_Films (id, idFilm, titre, dateParution, synopsis, splashImage, dateAjout, idMembreModification, actif) VALUES (2, 2, 'Usual Suspect', '1995-08-16', 'synopsis', 'usual-suspect.jpg', '2021-11-25', 2, '1');
INSERT INTO RM_ChangementsEnAttente_Films (id, idFilm, titre, dateParution, synopsis, splashImage, dateAjout, idMembreModification, actif) VALUES (3, 5, 'Resident Evil: Bienvenue � Raccon City', '2021-11-24', 'Autrefois le si�ge en plein essor du g�ant pharmaceutique Umbrella Corporation, Raccoon City est aujourd''hui une ville � l''agonie. L''exode de la soci�t� a laiss� la ville en friche... et un grand mal se pr�pare sous la surface. Lorsque celui-ci se d�cha�ne, les habitants de la ville sont � jamais... chang�s... et un petit groupe de survivants doit travailler ensemble pour d�couvrir la v�rit� sur Umbrella et survivre � la nuit.', 'resident-evil.jpg', '2021-11-25', 2, '0');
INSERT INTO RM_ChangementsEnAttente_Films (id, idFilm, titre, dateParution, synopsis, splashImage, dateAjout, idMembreModification, actif) VALUES (4, 5, 'Resident Evil: Bienvenue � Raccon City', '2021-11-24', 'Autrefois le si�ge en plein essor du g�ant pharmaceutique Umbrella Corporation, Raccoon City est aujourd''hui une ville � l''agonie. L''exode de la soci�t� a laiss� la ville en friche... et un grand mal se pr�pare sous la surface. Lorsque celui-ci se d�cha�ne, les habitants de la ville sont � jamais... chang�s... et un petit groupe de survivants doit travailler ensemble pour d�couvrir la v�rit� sur Umbrella et survivre � la nuit.', 'resident-evil.jpg', '2021-11-25', 2, '0');

INSERT INTO RM_Votes_Films_EnAttente VALUES (1, 1, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (2, 2, '0');
INSERT INTO RM_Votes_Films_EnAttente VALUES (1, 3, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (2, 3, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (3, 3, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (4, 3, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (5, 3, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (6, 3, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (7, 3, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (8, 3, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (9, 3, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (10, 3, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (1, 4, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (2, 4, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (3, 4, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (4, 4, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (5, 4, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (6, 4, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (7, 4, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (8, 4, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (9, 4, '1');
INSERT INTO RM_Votes_Films_EnAttente VALUES (10, 4, '1');

INSERT INTO RM_Messages_MurFilms (id, idMembre, idFilm, message) VALUES (1, 1, 3, 'Film excellent, � voir absolument!');
INSERT INTO RM_Messages_MurFilms (id, idMembre, idFilm, message) VALUES (2, 2, 4, 'Du grand Kassovitz..');
INSERT INTO RM_Messages_MurFilms (id, idMembre, idFilm, message) VALUES (3, 3, 4, 'D�routant!');
INSERT INTO RM_Messages_MurFilms (id, idMembre, idFilm, message) VALUES (4, 4, 1, 'Tr�s triste');
INSERT INTO RM_Messages_MurFilms (id, idMembre, idFilm, message) VALUES (5, 5, 1, 'Pas vu');

INSERT INTO RM_Votes_MurFilms VALUES (1, 1, '1');
INSERT INTO RM_Votes_MurFilms VALUES (2, 2, '1');
INSERT INTO RM_Votes_MurFilms VALUES (3, 3, '1');
INSERT INTO RM_Votes_MurFilms VALUES (4, 4, '1');
INSERT INTO RM_Votes_MurFilms VALUES (5, 5, '0');

INSERT INTO RM_FilmsFavoris_Membres VALUES (1, 3);
INSERT INTO RM_FilmsFavoris_Membres VALUES (2, 4);

INSERT INTO RM_Critiques (id, pointageScenario, pointageDistribution, pointageSFX, commentaire, dateAjout, idMembre, idFilm) VALUES (1, 8, 7, 9, 'Mathieu Kassovitz joue le jeu du vrai polar!', '2020-07-01', 1, 3);
INSERT INTO RM_Critiques (id, pointageScenario, pointageDistribution, pointageSFX, commentaire, dateAjout, idMembre, idFilm) VALUES (2, 9, 8, 7, 'Chronique d une banlieue', '2021-05-13', 2, 4);
INSERT INTO RM_Critiques (id, pointageScenario, pointageDistribution, pointageSFX, commentaire, dateAjout, idMembre, idFilm) VALUES (3, 4, 7, 9, 'Nouveau block-buster de l''horreur', '2021-11-27', 3, 5);


/*************************************************
  REQUETES
**************************************************/

ALTER SESSION SET NLS_DATE_FORMAT = 'yyyy-mm-dd';


-- 1.   Combien y a-t-il eu de films par genre au cours des 3 derniers mois?
SELECT
    COUNT(RM_Films.id) AS "Nombre de films par genre"
    ,RM_Genres.nom AS "Genre"
FROM RM_Films
INNER JOIN RM_GenresFilms
ON RM_Films.id = RM_GenresFilms.idFilm
INNER JOIN RM_Genres
ON RM_GenresFilms.idGenre = RM_Genres.id
WHERE (SYSDATE - RM_Films.dateParution) < 91
GROUP BY RM_Genres.nom;


-- 2.	Quels sont les films, en ordre de popularit�, dans le genre Drame ?
SELECT 
    RM_Films.titre AS "Titre"
    ,RM_Films.dateParution AS "Date de parution"
    ,RM_Films.synopsis AS "Synopsis"
    ,RM_Films.splashImage AS "Cover"
    ,COUNT(RM_Votes_MurFilms.pourContre) AS "Popularit�"
FROM RM_Films
INNER JOIN RM_GenresFilms
ON RM_Films.id = RM_GenresFilms.idFilm
INNER JOIN RM_Genres
ON RM_GenresFilms.idGenre = RM_Genres.id
INNER JOIN RM_Messages_MurFilms
ON RM_Films.id = RM_Messages_MurFilms.idFilm
INNER JOIN RM_Votes_MurFilms
ON RM_Messages_MurFilms.id = RM_Votes_MurFilms.idMessage
WHERE RM_Votes_MurFilms.pourContre LIKE '1' AND RM_Genres.nom LIKE 'Drame'
GROUP BY RM_Films.titre, RM_Genres.nom, RM_Films.dateParution, RM_Films.synopsis, RM_Films.splashImage
ORDER BY COUNT(RM_Votes_MurFilms.pourContre) DESC;


-- 3.	Quels films n�ont pas �t� critiqu�s au cours des derniers 6 mois ?
SELECT
    RM_Films.titre AS "Titre"
    ,RM_Films.dateParution AS "Date de parution"
    ,RM_Films.synopsis AS "Synopsis"
    ,RM_Films.splashImage AS "Cover"
FROM RM_Films
WHERE (RM_Films.id NOT IN (SELECT idFilm 
                            FROM RM_Critiques
                            WHERE (SYSDATE - RM_Critiques.dateAjout) < 181));

                       
-- 4.	Quelle est la liste des films dont les profits sont inf�rieurs � 3 fois le budget ?
SELECT
    RM_Films.titre AS "Titre"
    ,RM_Films.dateParution AS "Date de parution"
    ,RM_Films.synopsis AS "Synopsis"
    ,RM_Films.splashImage AS "Cover"
    ,RM_Films.profits
    ,RM_Films.budget
FROM RM_Films
WHERE RM_Films.profits < (RM_Films.budget * 3);


-- 5.	Cr�ez une vue qui listera les films qui ont �t� modifi�s 2 fois.
-- Pour qu�une modification soit valide, elle doit avoir obtenu au moins 10 votes � Pour � (Thumbs up).
CREATE VIEW vue_FilmsModifiesDeuxFois AS
SELECT
    *
FROM RM_Films
WHERE RM_Films.id = ( SELECT 
                        RM_ChangementsEnAttente_Films.idFilm
                        FROM RM_ChangementsEnAttente_Films
                        WHERE RM_ChangementsEnAttente_Films.actif = '0' AND 
                                RM_ChangementsEnAttente_Films.id IN (SELECT 
                                                                        RM_Votes_Films_EnAttente.idAttente 
                                                                        FROM RM_Votes_Films_EnAttente
                                                                        WHERE RM_Votes_Films_EnAttente.pourContre LIKE '1'
                                                                        GROUP BY idAttente
                                                                        HAVING COUNT(RM_Votes_Films_EnAttente.pourContre) > 9)
                        GROUP BY RM_ChangementsEnAttente_Films.idFilm
                        HAVING COUNT(RM_ChangementsEnAttente_Films.idFilm) = 2);
  
SELECT * FROM vue_FilmsModifiesDeuxFois;

                        
-- 6.	Listez les personnes qui ont particip� � au moins 2 com�dies dans la derni�re ann�e.
SELECT
    RM_Personnes.prenom AS "Prenom"
    ,RM_Personnes.nom AS "Nom"
FROM RM_Personnes
INNER JOIN RM_Employes
ON RM_Personnes.id = RM_Employes.idPersonne
INNER JOIN RM_Films
ON RM_Employes.idFilm = RM_Films.id
WHERE RM_Films.id IN (SELECT RM_GenresFilms.idFilm
                        FROM RM_GenresFilms
                        INNER JOIN RM_Genres
                        ON RM_GenresFilms.idGenre = RM_Genres.id
                        WHERE RM_Genres.nom LIKE 'Com_die')
GROUP BY RM_Personnes.prenom, RM_Personnes.nom
HAVING COUNT(RM_Films.id) > 1;
   
    
-- 7.Affichez le total des profits, par genre, pour la derniere annees. Les montants doivent etre affiches apres taxes,avec 2 chiffres apres la virgule et un $
-- 14.975% de taxes ...?
SELECT CONCAT(ROUND(SUM(RM_Films.Profits * 0.85025),2),' $') AS "Profits apres taxes", RM_Genres.Nom AS "Genre"
FROM RM_Films
    INNER JOIN RM_GenresFilms ON RM_Films.id = Rm_GenresFilms.idFilm
        INNER JOIN RM_Genres ON RM_Genres.id = RM_GenresFilms.idGenre       
WHERE RM_Films.dateParution > ADD_MONTHS(SYSDATE, -(1 * 12))
GROUP BY RM_Genres.Nom;        


-- 8. Quel sont les 10 meilleurs films de l'annee (Film revenant le plus souvent dans favoris des membres) (pour l'annee 2021)
SELECT RM_Films.Titre
FROM RM_Films
    INNER JOIN RM_FilmsFavoris_Membres ON RM_FilmsFavoris_Membres.idFilm = RM_Films.id
WHERE ROWNUM <= 10 AND RM_Films.dateParution BETWEEN '2021-1-1' AND '2021-12-31'
GROUP BY RM_Films.titre
ORDER BY COUNT(RM_FilmsFavoris_Membres.idFilm);


-- 9. Affichez la liste des acteurs avec le nombre de films dans lesquels ils ont joue en tant qu'acteur
SELECT CONCAT(RM_Personnes.prenom,' '|| RM_Personnes.nom) AS "Nom Complet", COUNT(RM_Employes.idFilm) AS "Nombre de film"
FROM RM_Personnes
    INNER JOIN RM_Employes ON RM_Personnes.id = RM_Employes.idPersonne
WHERE RM_Employes.idRole = (SELECT id FROM RM_Roles WHERE RM_Roles.nom LIKE 'Acteur')
GROUP BY RM_Personnes.prenom, RM_Personnes.nom
ORDER BY COUNT(RM_Employes.idFilm) DESC;


-- 10.	Quel genre a le plus de films ? Affichez seulement UN r�sultat.
SELECT
    *
FROM ( SELECT
            RM_Genres.nom AS "Genre"
            ,COUNT(RM_Films.id) AS "Nombre de films"
            ,ROW_NUMBER() OVER (ORDER BY COUNT(RM_Films.id) DESC) "Position"
        FROM RM_Films
        INNER JOIN RM_GenresFilms
        ON RM_Films.id = RM_GenresFilms.idFilm
        INNER JOIN RM_Genres
        ON RM_GenresFilms.idGenre = RM_Genres.id
        GROUP BY RM_Genres.nom)
WHERE "Position" = 1;


-- 11.	Quel pourcentage des membres vient du Canada ?
SELECT
    (COUNT(RM_Membres.id) / (SELECT COUNT(RM_Membres.id)
                               FROM RM_Membres) * 100) AS "Pourcentage de membres Canadiens"
FROM RM_Membres
INNER JOIN RM_Pays
ON RM_Membres.idPays = RM_Pays.id
WHERE RM_Pays.nom LIKE 'Canada'
GROUP BY RM_Membres.idPays;


-- 12 Lister les membres dont la 3e lettre du courriel est un A et qui habitent au Canada, en France ou en Belgique
SELECT RM_Membres.nom||', ' || SUBSTR(RM_Membres.prenom,1,1)||'.' AS "Membre"
FROM RM_Membres
WHERE LOWER(RM_Membres.email) LIKE '__a%' AND (RM_Membres.idPays = 'CA' OR RM_Membres.idPays = 'FR' OR RM_Membres.idPays = 'BE') ;


-- 13. Lister les personnes ayant une autre personne comme jumeau.
SELECT RM_Personnes.prenom ||' '||RM_Personnes.nom AS "Personnes qui ont un jumeau"
FROM  RM_Personnes
WHERE EXTRACT(MONTH FROM RM_Personnes.DateNaissance) = ANY(
            SELECT EXTRACT(MONTH FROM RM_Personnes.dateNaissance)
            FROM RM_personnes
            GROUP BY EXTRACT(MONTH FROM RM_personnes.dateNaissance)
            HAVING COUNT(EXTRACT(MONTH FROM RM_personnes.dateNaissance)) >=2)
            AND 
        EXTRACT(YEAR FROM RM_Personnes.DateNaissance) = ANY (
            SELECT EXTRACT(YEAR FROM RM_Personnes.dateNaissance)
            FROM RM_personnes
            GROUP BY EXTRACT(YEAR FROM RM_personnes.dateNaissance)
            HAVING COUNT(EXTRACT (YEAR FROM RM_personnes.dateNaissance)) >=2);
     